FROM ubuntu:20.04

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt update && apt install wget python make u-boot-tools gcc g++ git subversion -y
RUN wget https://uav.hds.utc.fr/src/toolchain/x86_64-meta-toolchain-flair-armv7a-neon.sh -q
RUN chmod +x x86_64-meta-toolchain-flair-armv7a-neon.sh
RUN ./x86_64-meta-toolchain-flair-armv7a-neon.sh -y
RUN rm ./x86_64-meta-toolchain-flair-armv7a-neon.sh
